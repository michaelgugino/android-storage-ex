package com.michaelgugino.testing1;

import android.content.Intent;
import android.database.Cursor;
import android.os.StatFs;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.content.res.AssetManager;
import android.widget.TextView;

import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.database.sqlite.SQLiteDatabase.OPEN_READWRITE;


public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    private void copyFile(InputStream inputFile, OutputStream outputFile, int blockSize) throws IOException {
        byte[] buffer = new byte[blockSize];
        int read;
        while((read = inputFile.read(buffer)) != -1){
            outputFile.write(buffer, 0, read);
        }
    }

    private void copyDB(AssetManager manager) {
        try {
            InputStream inputFile = manager.open("hello.db");
            File outFile = new File(getFilesDir(), "hello.db");
            // File outFile = new File(getExternalFilesDir(null), "hello.db");
            StatFs outStats = new StatFs(outFile.getParent());
            int blockSize = outStats.getBlockSize();
            System.out.println(blockSize);
            OutputStream outputFile = new FileOutputStream(outFile);
            copyFile(inputFile, outputFile, blockSize);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Called when the user taps the Send button */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    public void updateText(View view) {
        String message = "Couldn't open.";
        StringBuilder textBuilder = new StringBuilder();
        AssetManager assets = view.getContext().getAssets();
        try {
            InputStream stream1 = assets.open("file1.txt");
            int c = 0;
            while ((c = stream1.read()) != -1) {
                textBuilder.append((char) c);
            }
            stream1.close();
            message = textBuilder.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        File dbFile = new File(getFilesDir(), "hello.db");
        try {
            StatFs dbStats = new StatFs(dbFile.getPath());
        }
        catch (IllegalArgumentException e) {
            System.out.println("Caught statfs execption");
            copyDB(assets);
        }

        SQLiteDatabase db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, OPEN_READWRITE);
        String[] columns = {"msgtxt"};
        Cursor cursor = db.query("messages", columns, null, null, null, null, null);
        while(cursor.moveToNext()) {
            message += cursor.getString(0);
        }
        cursor.close();
        db.close();
        TextView textView2 = findViewById(R.id.textView2);
        textView2.setText(message);
        System.out.println("Button pressed");
    }
}